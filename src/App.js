import React from "react";
import Header from "./components/header/header";
import Routes from "./components/routes";


function App() {

    
  return (
    <div className="App">
        <Header/>
        <main className="main">
            <Routes/>
        </main>
        <footer className="footer">
        </footer>
    </div>
  );
}

export default App;
