import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {SkillsProvider} from "./providers/skillsProvider";
import {CountProvider} from "./providers/countProvider";
import {PortfolioProvider} from "./providers/portfolioProvider";

ReactDOM.render(
    <BrowserRouter>
        <PortfolioProvider>
            <SkillsProvider>
                <CountProvider>
                    <App/>
                </CountProvider>
            </SkillsProvider>
        </PortfolioProvider>
    </BrowserRouter>,
    document.getElementById('root')
);

