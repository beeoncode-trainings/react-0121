import React from "react"
import {Switch, Route, Redirect} from "react-router-dom"
import { isAuthRoutes, notAuthRoutes} from "../../routes"
import { HOME_PAGE, LOGIN_PAGE } from "../../utils/urls"

const Routes = () => {
    return  <Switch>
        {
            localStorage.getItem('token') ? isAuthRoutes.map(({id, path, Component}) => {
                return <Route key={id} path={path} component={Component} exact/>
            })
        :
            notAuthRoutes.map(({id, path, Component}) => {
                return <Route key={id} path={path} component={Component} exact/>
            })
        }
        <Redirect to={localStorage.getItem('token') ? HOME_PAGE : LOGIN_PAGE}/>
  </Switch>
}

export default Routes