import React from "react";
import {isAuthRoutes, notAuthRoutes} from "../routes";
import {Redirect, Route, Switch} from "react-router-dom";
import {HOME_PAGE, LOGIN_PAGE} from "../urls";

const Routes = () => {

    let token = localStorage.getItem('token')
    return <Switch>
        {
          token && isAuthRoutes.map(({path,component,id}) => {
                return <Route key={id} path={path} component={component} exact/>
            })
        }
        {
            notAuthRoutes.map(({path,component,id}) => {
                return <Route key={id} path={path} component={component} exact/>


            })
        }
        <Redirect to={token ? HOME_PAGE : LOGIN_PAGE}/>

    </Switch>
}



export default Routes
