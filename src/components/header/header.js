import React, {useState} from "react";
import style from "../header.module.css";
import logo from "../../img/logo.png";
import {GiHamburgerMenu} from "react-icons/gi";
import cn from "classnames"
import {NavLink} from "react-router-dom";
import {isAuthRoutes, notAuthRoutes} from "../../routes";
import Button from "../global/button/button";
import {LOGIN_PAGE} from "../../urls";
import {useHistory} from "react-router-dom"
import Search from "../search/search";
import Languages from "../languages/languages";

const  Header = () => {

    const history = useHistory()

 const [openMenu, setOpenMenu] = useState(false);

    const showMenu = () => {
        setOpenMenu(!openMenu);
    }

    const logout = () => {
        localStorage.removeItem('token')

        if (!localStorage.getItem('token')){
            history.push(LOGIN_PAGE)
            window.location.reload()
        }
    }
    return (
        <header className={style.header}>
            <div className={style.img}>
                <img src={logo}  alt="logo"/>
            </div>
            {localStorage.getItem('token') && <div>
                <Search/>
            </div>}
            <div>
                <Languages/>
            </div>
            <ul className={cn(style.headersUl, style.dddd)} style={{display: openMenu === true && "flex"}}>
                {
                    localStorage.getItem('token') ?isAuthRoutes.map(({id,name,path})=> {
                        return (
                            <li key={id}>
                            <NavLink to={path} exact>
                                    {name}
                            </NavLink>
                            </li>
                        )
                    })
                :
                    notAuthRoutes.map(({id,name,path})=> {
                        return (
                            <li key={id}>
                                <NavLink to={path} exact>
                                    {name}
                                </NavLink>
                            </li>
                        )
                    })
                }

                {localStorage.getItem('token') && <Button onClick={logout}>Logout</Button>}
            </ul>
            <div className={style.icon} onClick={showMenu}>
                <GiHamburgerMenu/>
            </div>
        </header>
    )
}
export default Header;