import React from 'react';
import css from "./authForm.module.css"

const AuthForm = ({children}) => {
    return (
        <div className={css.authForm}>
            {children}
        </div>
    );
};

export default AuthForm;