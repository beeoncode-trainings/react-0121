import React, {useState} from 'react';

const Languages = () => {
    const languages = [
        {id: 1, name: 'en'},
        {id: 2, name: 'am'},
        {id: 3, name: 'ru'},
    ]

    const [color, setColor] = useState({})

    return (
        <ul>
            {
                languages.map(({id, name}) => {
                    return <li
                        key={id}
                        style={{color: color[id] ? "darkblue" : "red"}}
                        onClick={() => setColor({[id]: true})}
                    >
                        {name}
                    </li>
                })
            }
        </ul>
    );
};

export default Languages;