import React from "react"
import {useCount} from "../../providers/countProvider";

const Contacts = () => {
    const {count, setCount} = useCount()



    return <div>
        <div>
            {count}
        </div>
        <div>
            <button onClick={() => setCount(count + 1)}>
                UP
            </button>
            <button onClick={() => setCount(count - 1)}>
                DOWN
            </button>
        </div>
    </div>
}

export default Contacts