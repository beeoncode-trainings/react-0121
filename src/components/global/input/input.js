import React from 'react';
import css from "./input.module.css"

const Input = ({cn, ...props}) => {



    return (
        <label>
            <input
                className={css[cn]}
                {...props}
            />
        </label>
    );
};

export default Input;