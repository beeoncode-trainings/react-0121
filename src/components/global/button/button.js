import React from 'react';
import css from './button.module.css'
import cnames from "classnames"

const Button = ({children, cn, ...props}) => {
    return (
        <button {...props} className={cnames(css.btn, css[cn])}>
            {children}
        </button>
    );
};

export default Button;