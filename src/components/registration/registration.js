import React, {useState} from 'react';
import AuthForm from "../authForm/authForm";
import css from "./registration.module.css"
import Input from "../global/input/input";
import Button from "../global/button/button";

import {useHistory, useLocation} from "react-router-dom"


const Registration = () => {

    const history = useHistory()

    const [userName, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')


    const changeUserName = (e) => {
        setUsername(e.target.value)
    }
    const changeEmail = (e) => {
        setEmail(e.target.value)
    }
    const changePassword = (e) => {
        setPassword(e.target.value)
    }
    const changeConfirmPassword = (e) => {
        setConfirmPassword(e.target.value)
    }

    const registration = (e) => {
        e.preventDefault()
        localStorage.setItem('user', JSON.stringify({
            userName,
            email,
            password,
            confirmPassword
        }))
        history.push('/login')
    }

    return (
        <div className={css.registration}>
            <AuthForm>
                <form onSubmit={registration}>
                    <Input
                        type="text"
                        cn="registerInput"
                        placeholder="Username"
                        value={userName}
                        onChange={changeUserName}
                    />
                    <Input
                        type="text"
                        cn="registerInput"
                        placeholder="Email"
                        value={email}
                        onChange={changeEmail}
                    />
                    <Input
                        type="password"
                        cn="registerInput"
                        placeholder="Password"
                        value={password}
                        onChange={changePassword}
                    />
                    <Input
                        type="password"
                        cn="registerInput"
                        placeholder="Password"
                        value={confirmPassword}
                        onChange={changeConfirmPassword}
                    />
                    <Button cn="registrationButton">
                        Registration
                    </Button>
                </form>

            </AuthForm>
        </div>

    );
};

export default Registration;