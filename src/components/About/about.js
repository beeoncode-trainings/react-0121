import React, {useContext} from 'react';
import {portfolioContext} from "../../state/state";
import {useCount} from "../../providers/countProvider";

const About = () => {

    const {text} = useCount()
    console.log(text)
    const portfolioState = useContext(portfolioContext)

    console.log(portfolioState)
    return (
        <div>
            {text}
        </div>
    );
};

export default About;