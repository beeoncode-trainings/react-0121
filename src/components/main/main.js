import React, {useState} from 'react';
import css from "./main.module.css"

const Main = () => {
    const skills = [
        {id: 1, skill: "HTML", percent: "90%"},
        {id: 2, skill: "CSS", percent: "80%"},
        {id: 3, skill: "SCSS", percent: "50%"},
        {id: 4, skill: "JavaScript", percent: "60%"},
        {id: 5, skill: "React js", percent: "10%"},
    ]


    const [showPercent, setShowPercent] = useState({})

    const show = (id) => {
        // setShowPercent({[id]: !showPercent[id]})

        setShowPercent(prevState => ({
            ...prevState,
            [id]: !showPercent[id]
        }))
    }

    console.log(showPercent)
    return (
        <main className={css.main}>
            <ul>
                {
                    skills.map(s => {
                        return <li key={s.id} onClick={() => show(s.id)}>
                            <span>{s.skill}</span>
                            {showPercent[s.id] && <span>{s.percent}</span>}
                        </li>
                    })
                }
            </ul>
        </main>
    );
};

export default Main;