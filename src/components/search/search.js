import React, {useState} from 'react';
import Input from "../global/input/input";

const Search = () => {

    const [search, setSearch] = useState('')
    return (
        <div>
            <Input
                cn="searchInput"
                value={search}
                onChange={e => setSearch(e.target.value)}
            />
        </div>
    );
};

export default Search;