import React, {useContext, useState} from "react"
import {portfolioContext} from "../../state/state";
import {usePortfolio} from "../../providers/portfolioProvider";
import Item from "../about/Item/Item";

const Portfolio = () => {

    const [subs, setSubs] = useState([])
    const [activeSub, setActiveSub] = useState(null)
    const {portfolios,portfolioTypes} = usePortfolio()



    const showSubCategory = (subCategory) => {
        setSubs(subCategory);
        setActiveSub(null)
        console.log(subCategory)
    }

    const showSubInfo = (name) => {
        setActiveSub(name)
    }
  return (
      <div>
          {
              portfolioTypes.map((item)=>{
                  return (
                      <Item key={item.id} onClick={() =>showSubCategory(item.sub) }>
                          {item.name}
                      </Item>
                  )
              })
          }

    <div>
        {subs.map(({id, name}) => {
            return (
                <Item key={id} onClick={() => showSubInfo(name)}>
                    {name}
                </Item>
            )
        })}
    </div>
          <div>
              {portfolios.map(({id,name,subcategory}) => {
                  return (
                      subcategory === activeSub ? <Item key={id}>
                          {name}
                      </Item> : null
                  )

              })}
          </div>
      </div>


  )
}


export default Portfolio