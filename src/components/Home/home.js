import React, {useContext, useEffect, useState} from 'react';
import {objectBContext} from "../../state/state";

const Home = () => {

    const b = useContext(objectBContext)
    console.log(b)

    const [dropdown, setDropdown] = useState(false)

    const [count, setCount] = useState(1)
    const [color, setColor] = useState('green')

    const [borderRadius, setBorderRadius] = useState('none')

    useEffect(() => {
        if (dropdown){
            setBorderRadius('50%')
        } else {
            setBorderRadius('0')
        }
        console.log(dropdown, borderRadius)
    }, [dropdown])

    // const [text, setText] = useState('Armenia')

    // const [width, setWidth] = useState(window.innerWidth)
    //
    // const changeWidth = () => {
    //     setWidth(window.innerWidth)
    // }

    // useEffect(() => {
    //     window.addEventListener('resize', changeWidth)
    //
    //     return () => removeEventListener('resize', changeWidth)
    // }, [])


    useEffect(() => {
        switch (count){
            case 2:
                setColor("blue")
                break
            case 3:
                setColor("yellow")
                break
            case 4:
                setColor("maroon")
                break
            case 5:
                setColor("pink")
                break
            case 6:
                setColor("purple")
                break
            default:
                setColor('green')
        }
    }, [count])

    return (
        <div>
            <div
                onClick={() => setCount(count + 1)}
                style={{
                    fontSize: "64px",
                    width: "100px",
                    height: "100px",
                    background: color,
                    color: "orange",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    margin: "20px auto",
                    cursor: "pointer",
                    borderRadius: borderRadius
                }}
            >
                {count}
            </div>

            {/*<div onClick={() => setText('Yerevan')}>*/}
            {/*    {text}*/}
            {/*</div>*/}

            {/*<div>*/}
            {/*    {width}*/}
            {/*</div>*/}

            <div onClick={() => setDropdown(!dropdown)}>
                dropdown
            </div>

            {
                dropdown && <div>
                    1, 2, 3, 4, 5
                </div>
            }

        </div>
    );
};

export default Home;