import {ABOUT_PAGE, HOME_PAGE, LOGIN_PAGE, REGISTRATION_PAGE} from "./urls";
import Home from './components/home/home'
import About from "./components/about/about";
import Login from "./components/login/login";
import Registration from "./components/Registration/registration";
import Portfolio from "./components/portfolio/portfolio";
import {CONTACTS_PAGE, PORTFOLIO_PAGE, SKILLS_PAGE} from "./utils/urls";
import Skills from "./components/skills/skills";
import Contacts from "./components/contacts/contacts";

export const isAuthRoutes = [
    {
        component: Home,
        id: 1,
        path: HOME_PAGE,
        name: 'Home'
    }, {
        component: About,
        id: 2,
        path: ABOUT_PAGE,
        name: 'About'
    },
    {
        component: Portfolio,
        id: 5,
        path: PORTFOLIO_PAGE,
        name: 'Portfolio'
    },
    {
        component: Skills,
        id: 6,
        path: SKILLS_PAGE,
        name: 'Skills'
    },
    {
        component: Contacts,
        id: 7,
        path: CONTACTS_PAGE,
        name: 'Contacts'
    }
]

export const notAuthRoutes = [

    {
        component: Login,
        id: 3,
        path: LOGIN_PAGE,
        name: 'Login'
    }, {
        component: Registration,
        id: 4,
        path: REGISTRATION_PAGE,
        name: 'Registration'
    }

]

