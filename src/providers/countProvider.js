import React, {createContext, useContext, useState} from "react";

const CountContext = createContext({})

const CountProvider = ({children}) => {
    const [count, setCount] = useState(0)
    const [text, setText] = useState('Armenia')

    return <CountContext.Provider value={{count, setCount, text}}>
        {children}
    </CountContext.Provider>
}

const useCount = () => {
    return useContext(CountContext)
}

export {CountProvider, useCount}